interface GetOrcidAuthorsValidResponse {
  status: 'ok';
  'message-version': '1.0.0';
  'message-type': 'work-list';
  message: {
    'total-results': number;
    items: Array<{
      title: string[];
      DOI: string;
      author: Array<{
        given: string;
        family: string;
        ORCID?: string;
        'authenticated-orcid'?: boolean;
      }>;
    }>;
  };
}

interface GetOrcidAuthorsInvalidResponse {
  status: 'error';
}

export type GetOrcidAuthorsResponse = GetOrcidAuthorsValidResponse | GetOrcidAuthorsInvalidResponse;

export function isValidOrcidAuthorsResponse(response: GetOrcidAuthorsResponse)
: response is GetOrcidAuthorsValidResponse {
  return (response as GetOrcidAuthorsValidResponse).status === 'ok';
}
