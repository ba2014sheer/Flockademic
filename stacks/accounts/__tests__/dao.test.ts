import * as dao from '../src/dao';

const mockTransaction = {
  batch: jest.fn(),
  none: jest.fn(),
};
const mockedDatabase: any = {
  any: jest.fn(),
  manyOrNone: jest.fn().mockReturnValue(Promise.resolve(null)),
  none: jest.fn(),
  one: jest.fn().mockReturnValue(Promise.resolve({ account_id: 'arbitrary user id' })),
  oneOrNone: jest.fn().mockReturnValue(Promise.resolve(null)),
  tx: (callback) => callback(mockTransaction),
};

// tslint:disable-next-line:max-line-length
it('should insert a UUID and its corresponding refresh token into the database without expecting a return value', () => {
  dao.insertSessionUuid(mockedDatabase, 'some uuid', 'some refresh token');

  expect(mockedDatabase.none.mock.calls.length).toBe(1);
  expect(mockedDatabase.none.mock.calls[0][1])
    .toEqual({ identifier: 'some uuid', refreshToken: 'some refresh token' });
});

it('should fetch the profiles associated with account identifiers', () => {
  dao.getProfilesForAccounts(mockedDatabase, [ 'account ID 1', 'account ID 2' ]);

  expect(mockedDatabase.manyOrNone.mock.calls.length).toBe(1);
  expect(mockedDatabase.manyOrNone.mock.calls[0][1]).toEqual([ ['account ID 1', 'account ID 2' ] ]);
});

it('should fetch the session associated with a refresh token', () => {
  dao.getSessionForToken(mockedDatabase, 'some refresh token');

  expect(mockedDatabase.one.mock.calls.length).toBe(1);
  expect(mockedDatabase.one.mock.calls[0][1]).toEqual({ refreshToken: 'some refresh token' });
});

it('should only return the fetched ID', () => {
  mockedDatabase.one.mockReturnValueOnce(Promise.resolve({ session_id: 'some_id' }));

  return expect(dao.getSessionForToken(mockedDatabase, 'arbitrary refresh token'))
    .resolves.toEqual({ identifier: 'some_id' });
});

it('should check whether an account exists for a given ORCID', () => {
  mockedDatabase.oneOrNone.mockReturnValueOnce(Promise.resolve({ identifier: 'some_id' }));

  return expect(dao.getAccountForOrcid(mockedDatabase, 'arbitrary_orcid'))
    .resolves.toEqual({ identifier: 'some_id' });
});

it('should insert an ORCID\'s data and a master account without expecting a return value', () => {
  dao.storeOrcidCredentials(
    mockedDatabase, 'orcid', 'access_token', 'refresh_token', 'session_id', 'account_id'
  );

  expect(mockTransaction.none.mock.calls.length).toBe(3);
  expect(mockTransaction.none.mock.calls[0][0]).toMatch(/^INSERT INTO orcids/);
  expect(mockTransaction.none.mock.calls[0][1]).toEqual({
    accessToken: 'access_token',
    orcid: 'orcid',
    refreshToken: 'refresh_token',
  });
  expect(mockTransaction.none.mock.calls[1][0]).toMatch(/^INSERT INTO accounts/);
  expect(mockTransaction.none.mock.calls[1][1]).toEqual({
    identifier: 'account_id',
    orcid: 'orcid',
  });
  expect(mockTransaction.none.mock.calls[2][0]).toMatch(/^UPDATE sessions/);
  expect(mockTransaction.none.mock.calls[2][1]).toEqual({
    accountId: 'account_id',
    sessionId: 'session_id',
  });
});
