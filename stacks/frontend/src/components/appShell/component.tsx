require('./styles.scss');

import * as React from 'react';
import { OutboundLink } from 'react-ga';
import { Link, Route, Switch } from 'react-router-dom';

import { AlertCallout } from '../alertCallout/component';
import { ArticleInitialisationPage } from '../articleInitialisationPage/component';
import { ArticleManagementPage } from '../articleManagementPage/component';
import { ArticleOverviewPage } from '../articleOverviewPage/component';
import { DashboardPage } from '../dashboardPage/component';
import { IndexPage } from '../indexPage/component';
import { JournalIndexPage } from '../journalIndexPage/component';
import { JournalInitialisationPage } from '../journalInitialisationPage/component';
import { JournalManagementPage } from '../journalManagementPage/component';
import { JournalOverviewPage } from '../journalOverviewPage/component';
import { MaintenanceModePage } from '../maintenanceModePage/component';
import { NotFound } from '../notFound/component';
import { OrcidButton } from '../orcidButton/component';
import { PrerenderableRouter } from '../prerenderableRouter/component';
import { ProfileOverviewPage } from '../profileOverviewPage/component';
import { ReactGAListener } from '../reactGAListener/component';
import { Spinner } from '../spinner/component';

const orcidIcon = require('../../img/orcid_logo.svg');

interface AppShellState {
  isMobileMenuOpen: boolean;
}

export class AppShell extends React.Component<{ renderIndex?: boolean }, AppShellState> {
  public constructor(
    // These defaults are only relevant for production compilation, i.e. can't be tested.
    // The double ignore comment is needed because Istanbul sees two if statements there after compilation.
    props /* istanbul ignore next */ = /* istanbul ignore next */ { renderIndex: true }) {
    super(props);

    this.state = {
      isMobileMenuOpen: false,
    };

    this.toggleMobileMenu = this.toggleMobileMenu.bind(this);
  }

  public render() {
    // The top-level element should usually be the index page.
    // However, when prerendering for production, we want to be able to render an empty shell for fallback routes.
    const TopLevelElement =
      (typeof document !== 'undefined' || /* istanbul ignore next */ this.props.renderIndex)
      ? IndexPage
      // istanbul ignore next: only for production compilation, so we can't test this
      : () => <Spinner/>;

    return (
      <PrerenderableRouter>
        <ReactGAListener>
          <div className="appShell">
            <a className="skipLink" href="#content">Skip to Content</a>
            {renderEnvironmentWarning()}
            <header className="appHeader container" role="banner">
              <nav className="navbar" role="navigation" aria-label="main navigation">
                <div className="navbar-brand">
                  <Link className="navbar-item" to="/">
                    Flockademic&nbsp;<small>(beta)</small>
                  </Link>
                  <a
                    onClick={this.toggleMobileMenu}
                    href=""
                    aria-haspopup={true}
                    aria-expanded={this.state.isMobileMenuOpen}
                    aria-controls="mainMenu"
                    aria-label="Menu"
                    title="Menu"
                    className={this.getMobileMenuActiveClass('burger navbar-burger')}
                  >
                    <span/>
                    <span/>
                    <span/>
                  </a>
                </div>
                {this.renderMenu()}
              </nav>
            </header>
            <main id="content" role="main" tabIndex={-1}>
              <AlertCallout/>
              <Switch>
                {this.renderMaintenanceModePage()}
                {/* Note: we show the Spinner instead of the IndexPage when prerendering, */}
                {/* since React might otherwise make mistakes when reconciliating. */}
                <Route
                  exact={true}
                  path="/"
                  component={TopLevelElement}
                />
                <Route exact={true} path="/journals" component={JournalIndexPage}/>
                <Route exact={true} path="/journals/new" component={JournalInitialisationPage}/>
                {/* For the reason for the cast to any, see https://stackoverflow.com/q/48044558 */}
                <Route exact={true} path="/journal/:slug/manage" component={JournalManagementPage as any}/>
                <Route exact={true} path="/journal/:periodicalSlug/submit" component={ArticleInitialisationPage}/>
                <Route exact={true} path="/articles/new/:orcidWorkId?" component={ArticleInitialisationPage}/>
                {/* For the reason for the cast to any, see https://stackoverflow.com/q/48044558 */}
                <Route exact={true} path="/article/:articleId/manage" component={ArticleManagementPage as any}/>
                <Route exact={true} path="/article/:articleId" component={ArticleOverviewPage}/>
                <Route exact={true} path="/journal/:slug" component={JournalOverviewPage}/>
                {/* For the reason for the cast to any, see https://stackoverflow.com/q/48044558 */}
                {/* Note that when no ORCID is specified, the current user's profile will be loaded if possible */}
                <Route exact={true} path="/profile/:orcid?" component={ProfileOverviewPage as any}/>
                <Route exact={true} path="/dashboard" component={DashboardPage}/>
                <Route component={NotFound}/>
              </Switch>
            </main>
            <footer role="contentinfo" className="footer">
              <div className="container">
                <p className="content is-pulled-left">
                  {/* tslint:disable-next-line:max-line-length */}
                  Powered by <Link to="/" title="Flockademic homepage">Flockademic</Link>, an effort to help researchers achieve <OutboundLink to="https://fairoa.org/" eventLabel="fairoa" title="Fair Open Access principles">Fair Open Access</OutboundLink>.
                </p>
                <p className="content is-pulled-right">
                  <OutboundLink
                    to="https://medium.com/flockademic"
                    eventLabel="blog"
                    title="Flockademic weblog"
                  >
                    Blog
                  </OutboundLink>
                  &nbsp;|&nbsp;
                  <OutboundLink
                    to="https://tinyletter.com/Flockademic"
                    eventLabel="newsletter"
                    title="Flockademic newsletter"
                  >
                    Newsletter
                  </OutboundLink>
                  &nbsp;|&nbsp;
                  <OutboundLink
                    to="https://twitter.com/Flockademic"
                    eventLabel="twitter"
                    title="Flockademic on Twitter"
                  >
                    Twitter
                  </OutboundLink>
                  &nbsp;|&nbsp;
                  <OutboundLink
                    to="https://gitlab.com/Flockademic/Flockademic"
                    eventLabel="gitlab"
                    title="Flockademic source code"
                  >
                    Source code
                  </OutboundLink>
                </p>
              </div>
            </footer>
          </div>
        </ReactGAListener>
      </PrerenderableRouter>
    );
  }

  private renderMaintenanceModePage() {
    if (!process.env.MAINTENANCE_MODE) {
      return null;
    }

    return (
      <Route
        path="/"
        component={MaintenanceModePage}
      />
    );
  }

  private renderMenu() {
    if (process.env.MAINTENANCE_MODE) {
      return null;
    }

    return (
      <div
        id="mainMenu"
        className={this.getMobileMenuActiveClass('navbar-menu')}
      >
        <div className="navbar-end">
          <div className="navbar-item">
            <Link
              to="/dashboard"
              title="View your journals"
              className="button is-text"
            >
              Dashboard
            </Link>
          </div>
          <div className="navbar-item">
            <OrcidButton className="button is-text">
              <span className="icon">
                <img src={orcidIcon} alt=""/>
              </span>
              <span>Create account / sign in</span>
            </OrcidButton>
          </div>
        </div>
      </div>
    );
  }

  private toggleMobileMenu(event: React.MouseEvent<HTMLAnchorElement>) {
    event.preventDefault();

    return this.setState((prevState) => ({ isMobileMenuOpen: !prevState.isMobileMenuOpen }));
  }

  private getMobileMenuActiveClass(otherClasses: string) {
    return this.state.isMobileMenuOpen
      ? `is-active ${otherClasses}`
      : otherClasses;
  }
}

function renderEnvironmentWarning() {
  if (process.env.CODE_BRANCH === 'master') {
    return null;
  }

  return (
    <div className="notification is-marginless has-text-centered is-danger environment-warning">
      {/* tslint:disable-next-line:max-line-length */}
      You are browsing a version of Flockademic meant for testing (<var>{process.env.CODE_BRANCH}</var>). Do not use for actual data — instead, visit <OutboundLink to="https://Flockademic.com" eventLabel="to-production" title="Back to safety">Flockademic.com</OutboundLink>.
    </div>
  );
}
