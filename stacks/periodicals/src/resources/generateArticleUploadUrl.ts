import {
  PostGenerateUploadUrlForScholarlyArticleRequest,
  PostGenerateUploadUrlForScholarlyArticleResponse,
} from '../../../../lib/interfaces/endpoints/periodical';
import { Request } from '../../../../lib/lambda/faas';
import { getUploadUrl } from '../../../../lib/lambda/getUploadUrl';
import { DbContext } from '../../../../lib/lambda/middleware/withDatabase';
import { SessionContext } from '../../../../lib/lambda/middleware/withSession';
import { addFileToScholarlyArticle } from '../services/addFileToScholarlyArticle';

export async function generateArticleUploadUrl(
  context: Request<PostGenerateUploadUrlForScholarlyArticleRequest> & DbContext & SessionContext,
): Promise<PostGenerateUploadUrlForScholarlyArticleResponse> {
  if (context.session instanceof Error) {
    throw new Error('You do not appear to be logged in.');
  }

  if (typeof process.env.article_upload_bucket === 'undefined') {
    // tslint:disable-next-line:no-console
    console.log('No upload bucket defined.');
    throw new Error('There was a problem uploading your article.');
  }

  if (!context.body || !context.body.targetCollection || !context.body.targetCollection.identifier
    || !context.body.result || !context.body.result.associatedMedia || !context.body.result.associatedMedia.name) {
    throw(new Error('No article for which to upload a file specified.'));
  }
  const articleId = context.body.targetCollection.identifier;
  const filename = context.body.result.associatedMedia.name;

  if (!context.body.result.associatedMedia.license
    || context.body.result.associatedMedia.license !== 'https://creativecommons.org/licenses/by/4.0/') {
    throw new Error('Please agree to distribute the file under a CC-BY license.');
  }

  const bucketObjectName = `${articleId}_${filename}_${(new Date()).toISOString()}.pdf`;
  const uploadBucket = (process.env as any).article_upload_bucket;
  const uploadUrl = getUploadUrl(uploadBucket, bucketObjectName);
  const downloadUrl =
    `https://s3.${(process.env as any).aws_region}.amazonaws.com/${uploadBucket}/`
    + encodeURIComponent(bucketObjectName);

  await addFileToScholarlyArticle(
    context.database,
    context.session,
    articleId,
    downloadUrl,
    filename,
  );

  return {
    object: {
      toLocation: uploadUrl,
    },
    result: {
      associatedMedia: {
      contentUrl: downloadUrl,
      license: context.body.result.associatedMedia.license,
      name: filename,
    },
  } };
}
